// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "HomeWork_22v1GameMode.h"
#include "HomeWork_22v1/Game/HomeWork_22v1PlayerController.h"
#include "HomeWork_22v1/Character/HomeWork_22v1Character.h"
#include "UObject/ConstructorHelpers.h"

AHomeWork_22v1GameMode::AHomeWork_22v1GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AHomeWork_22v1PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}