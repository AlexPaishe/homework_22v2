// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "HomeWork_22v1GameMode.generated.h"

UCLASS(minimalapi)
class AHomeWork_22v1GameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AHomeWork_22v1GameMode();
};



