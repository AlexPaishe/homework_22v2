// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "HomeWork_22v1.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, HomeWork_22v1, "HomeWork_22v1" );

DEFINE_LOG_CATEGORY(LogHomeWork_22v1)
 